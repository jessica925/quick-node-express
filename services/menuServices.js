const {
  select,
  selectWhere,
  selectOne,
  insert,
  update,
  remove
 } = require("../model/menuModel");


class MenuServices {
  constructor() {}
  async getMenuListByIds(menuIds) {
       const menuListResult = await selectWhere(menuIds);
    const menuList = menuListResult.results && menuListResult.results;
    const jsonObj = {
      status: 0,
      msg: "授权菜单列表",
      data:menuList
    };
    return jsonObj;
  }
  async getMenuList() {    
    const menuListResult = await select();
    const menuList = menuListResult.results && menuListResult.results;
    const jsonObj = {
      status: 0,
      msg: "菜单列表",
      data:menuList
    };
    return jsonObj;
  }
  async getMenuDetail(id){
    const menuResult=await selectOne(id)
    console.log('22222222222');
    const menuDetail = menuResult.results && menuResult.results[0];
    const jsonObj = {
      status: 0,
      msg: "菜单详情",
      data:menuDetail
    };
    return jsonObj
  }
  async addMenu(menu) {
    await insert(menu);
    const jsonObj = {
      status: 0,
      msg: "添加菜单成功",
      data: null,
    };
    return jsonObj;
  }
  async updateMenu(menu) {
    await update(menu);
    const jsonObj = {
      status: 0,
      msg: "修改菜单成功",
      data: null,
    };
    return jsonObj;
  }
  async deleteMenu(id) {
    await remove(id);
    const jsonObj = {
      status: 0,
      msg: "删除菜单成功",
      data: null,
    };
    return jsonObj;
  }
}
module.exports = new MenuServices();
