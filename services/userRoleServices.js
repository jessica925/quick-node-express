const {
  selectWhere
 } = require("../model/userRoleModel");

class UserRoleServices {
  constructor() {}
  async getUserRoleListByUserId(userId) {    
    const userRoleListResult = await selectWhere('user_id',userId)
    const userRoleList = userRoleListResult.results && userRoleListResult.results;
    return userRoleList
  }
  async getAssignedUserList(userId) {    
    const userRoleListResult = await selectWhere('user_id',userId)
    const userRoleList = userRoleListResult.results && userRoleListResult.results;
    const jsonObj = {
      status: 0,
      msg: "分配的用户列表",
      data:userRoleList
    };
    return jsonObj;
  }
  async allotUser(userRole) {
    await insert(userRole);
    const jsonObj = {
      status: 0,
      msg: "分配用户成功",
      data: null,
    };
    return jsonObj;
  }

  async unassignUser(id) {
    await remove(id);
    const jsonObj = {
      status: 0,
      msg: "取消分配用户成功",
      data: null,
    };
    return jsonObj;
  }
}
module.exports = new UserRoleServices();
