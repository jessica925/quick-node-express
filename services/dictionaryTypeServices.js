const {
  select,
  insert,
  update,
  remove
 } = require("../model/dictionaryTypeModel");

class DictionaryTypeServices {
  constructor() {}
  async getDictionaryTypeList() {    
    const dictionaryTypeListResult = await select();
    const dictionaryTypeList = dictionaryTypeListResult.results && dictionaryTypeListResult.results;
    const jsonObj = {
      status: 0,
      msg: "字典分类列表",
      data:dictionaryTypeList
    };
    return jsonObj;
  }
  async addDictionaryType(dictionaryType) {
    await insert(dictionaryType);
    const jsonObj = {
      status: 0,
      msg: "添加字典分类成功",
      data: null,
    };
    return jsonObj;
  }
  async updateDictionaryType(dictionaryType) {
    await update(dictionaryType);
    const jsonObj = {
      status: 0,
      msg: "修改字典分类成功",
      data: null,
    };
    return jsonObj;
  }
  async deleteDictionaryType(id) {
    await remove(id);
    const jsonObj = {
      status: 0,
      msg: "删除字典分类成功",
      data: null,
    };
    return jsonObj;
  }
}
module.exports = new DictionaryTypeServices();
