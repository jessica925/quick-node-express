const {
  selectWhere,
  removeWhere,
  insert
 } = require("../model/roleMenuModel");

class RoleMenuServices {
  constructor() {}
  async getRoleMenuListByRoleIds(roleIds) {    
    const roleMenuListResult = await selectWhere(roleIds)
    const roleMenuList = roleMenuListResult.results && roleMenuListResult.results;
    const jsonObj = {
      status: 0,
      msg: "分配的菜单列表",
      data:roleMenuList
    };
    return jsonObj;
  }
  async getAssignedUserList(userId) {    
    const userRoleListResult = await selectWhere('user_id',userId)
    const userRoleList = userRoleListResult.results && userRoleListResult.results;
    const jsonObj = {
      status: 0,
      msg: "分配的用户列表",
      data:userRoleList
    };
    return jsonObj;
  }
  async bindMenuForRole(roleId,menuIds) {
    await removeWhere(roleId)
    const menuIdArr=menuIds.split(',')
    menuIdArr.forEach(async (menuId)=>{
       const roleMenu={roleId,menuId}
       await insert(roleMenu);
    }) 
    const jsonObj = {
      status: 0,
      msg: "角色权限分配成功",
      data: null,
    };
    return jsonObj;
  }
}
module.exports = new RoleMenuServices();
