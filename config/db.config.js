module.exports = {
  debug: false, //开启调试
  host: "110.42.130.88",
  port: "3306",
  user: "quick1",
  password: "quick",
  database: "quick1",
  timezone: "08:00",
  dateStrings:true,
  stringifyObjects: true, //是否序列化对象
  // acquireTimeout: 1500, // 连接超时时间 可以不写
  // connectionLimit: 10, // 最大连接数 默认属性值为10. 可以不写
  // waitForConnections: true, // 超过最大连接时排队 可以不写
  // queueLimit: 0, // 排队最大数量(0 代表不做限制)  可以不写
  // 用于指定允许挂起的最大连接数，如果挂起的连接数超过该数值，就会立即抛出一个错误，默认属性值为0.代表不允许被挂起的最大连接数。
};
