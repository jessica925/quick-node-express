const {
  getPermissionMenuListByRoleId,
  getRoleList,
  addRole,
  updateRole,
  deleteRole,
  assignPermission,
} = require("../services/roleServices");
class RoleController {
  constructor() {}
  getMenuPermission(body){
    const {id:roleId}=body
    return getPermissionMenuListByRoleId(roleId)
  }
  getList() {
    return getRoleList();
  }
  add(body) {
    const role = body;
    return addRole(role);
  }
  update(body) {
    const role = body;
    return updateRole(role);
  }
  remove(body) {
    const { id } = body;
    return deleteRole(id);
  }
  assignMenu(body) {
    const { roleId,menuIds } = body;
    return assignPermission(roleId,menuIds);
  }
}
module.exports = new RoleController();
