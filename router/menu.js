const express = require("express");
const router = express.Router();
const {
  getRoleMenuLsit,
  getPermission,
  getList,
  add,
  update,
  remove,
  assign,
} = require("../controllers/menuController");

router.get("/api/menu/getList", (req, res) => {
  const { query } = req;
  getList(query).then((data) => {
    console.log('data',data);
    res.send(data);
  });
});
router.post("/api/menu/add", (req, res) => {
  const { body } = req;
  add(body).then((data) => {
    res.send(data);
  });
});
router.post("/api/menu/update", (req, res) => {
  const { body } = req;
  update(body).then((data) => {
    res.send(data);
  });
});
router.post("/api/menu/delete", (req, res) => {
  const { body } = req;
  remove(body).then((data) => {
    res.send(data);
  });
});



module.exports = router;
