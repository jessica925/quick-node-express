const express = require("express");
const router = express.Router();
const {
  getPermission,
  getPageList,
  getDetail,
  getInfo,
  add,
  update,
  remove,
  batchRemove,
  updatePassword,
  resetPassword,
  enabled,
  disable,
} = require("../controllers/userController");

router.get("/api/user/getPermission", (req, res) => {
  const { query } = req;
  getPermission(query).then((data) => {
    res.send(data);
  });
});
router.get("/api/user/getPageList", (req, res) => {
  const { query } = req;
  getPageList(query).then((data) => {
    res.send(data);
  });
});
router.get("/api/user/getDetail", (req, res) => {
  const { query } = req;
  getDetail(query).then((data) => {
    res.send(data);
  });
});
router.get("/api/user/getInfo", (req, res) => {
  const { query } = req;
  getInfo(query).then((data) => {
    res.send(data);
  });
});
router.post("/api/user/add", (req, res) => {
  const { body } = req;
  add(body).then((data) => {
    res.send(data);
  });
});
router.post("/api/user/update", (req, res) => {
  const { body } = req;
  update(body).then((data) => {
    res.send(data);
  });
});
router.post("/api/user/delete", (req, res) => {
  const { body } = req;
  remove(body).then((data) => {
    res.send(data);
  });
});
router.post("/api/user/batchRemove", (req, res) => {
  const { body } = req;
  batchRemove(body).then((data) => {
    res.send(data);
  });
});
router.post("/api/user/updatePassword", (req, res) => {
  const { body } = req;
  updatePassword(body).then((data) => {
    res.send(data);
  });
});
router.post("/api/user/resetPassword", (req, res) => {
  const { body } = req;
  resetPassword(body).then((data) => {
    res.send(data);
  });
});
router.post("/api/user/enabled", (req, res) => {
  const { body } = req;
  console.log('enabled-body',body);
  enabled(body).then((data) => {
    res.send(data);
  });
});
router.post("/api/user/disable", (req, res) => {
  const { body } = req;
  disable(body).then((data) => {
    res.send(data);
  });
});
module.exports = router;
