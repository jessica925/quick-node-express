'use strict';
const db = require("../config/dbPool");

class RoleModel {
  selectWhere(roleIds){
    const sql = `select * from per_role_menus where role_id in(${roleIds})`;
    const sqlArr = [];
    return db.queryAsync(sql, sqlArr);
  }
  insert(roleMenu) {
    const sql =
      "insert into per_role_menus(role_id,menu_id) values(?,?)";
    const sqlArr = [
      roleMenu.roleId,
      roleMenu.menuId,
    ];
    return db.queryAsync(sql, sqlArr);
  }
  removeWhere(roleId) {
    const sql = "delete from per_role_menus where role_id=?";
    const sqlArr = [roleId];
    return db.queryAsync(sql, sqlArr);
  }
}
module.exports = new RoleModel();
