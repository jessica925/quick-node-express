const db = require("../config/dbPool");
class UserModel {
  selectMenu(roleId){
    const sql = `select * from sys_users  where roleId=? `;
    const sqlArr = [roleId];
    return db.queryAsync(sql, sqlArr);
  }
  selectOneWhere(fields, values) {
    let whereStr = "deleted=0";
    fields.forEach((field, index) => {
      whereStr += ` and ${field}='${values[index]}'`;
    });
    const sql = `select * from sys_users  where ${whereStr} `;
    const sqlArr = [];
    return db.queryAsync(sql, sqlArr);
  }
  selectTotal(userName) {
    let whereStr = ``;
    if (userName) {
      whereStr = `${whereStr} and user_name like '%${userName}%'`;
    }
    const sql = `select count(user_id) as total from sys_users where deleted=0 ${whereStr}`;
    const sqlArr = [];
    return db.queryAsync(sql, sqlArr);
  }
  selectPage(current, size, userName) {
    let whereStr = ``;
    const sqlArr = [];
    if (userName) {
      whereStr = `${whereStr} and user_name like '%${userName}%'`;
    }
    sqlArr.push(current);
    sqlArr.push(size);
    const sql = `select * from sys_users where deleted=0 ${whereStr} order by id desc limit ?,?`;
    return db.queryAsync(sql, sqlArr);
  }
  insert(user) {
    const sql =
      "insert into sys_users(user_id,user_name,password,full_name,phone,email,address,create_time,deleted,enabled,remark) values(?,?,?,?,?,?,?,?,?,?,?)";
    const sqlArr = [
      user.userId,
      user.userName,
      user.password,
      user.fullName,
      user.phone,
      user.email,
      user.address,
      user.create_time,
      0,
      0,
      user.remark,
    ];
    return db.queryAsync(sql, sqlArr);
  }
  update(user) {
    const sql =
      "update sys_users set user_name=?,full_name=?,phone=?,email=?,address=?,remark=? where id=?";
    const sqlArr = [
      user.userName,
      user.fullName,
      user.phone,
      user.email,
      user.address,
      user.remark,
      user.id,
    ];
    return db.queryAsync(sql, sqlArr);
  }
  updateField(ids, fields, values) {
    let setStr = "set ";
    fields.forEach((field, index) => {
      setStr += `${field}='${values[index]}'`;
    });
    const sql = `update sys_users ${setStr} where deleted=0 and id in (${ids})`;
    const sqlArr = [];
    return db.queryAsync(sql, sqlArr);
  }
  remove(ids) {
    // const sql=`update sys_users set deleted=0 where id in (${ids})`//逻辑删除
    const sql = `delete from sys_users where deleted=0 and id in (${ids})`; //物理删除
    const sqlArr = [];
    return db.queryAsync(sql, sqlArr);
  }
}
module.exports = new UserModel();
