'use strict';
const db = require("../config/dbPool");

class DictionaryTypeModel {
  select() {
    const sql = "select * from sys_dictionaries_type";
    const sqlArr = [];
    return db.queryAsync(sql, sqlArr);
  }
  insert(dic) {
    const sql =
      "insert into sys_dictionaries_type(dic_type_id,name) values(?,?)";
    const sqlArr = [
      dic.dicTypeId,
      dic.name,
    ];
    return db.queryAsync(sql, sqlArr);
  }
  update(dic) {
    const sql =
      "update sys_dictionaries_type set dic_type_id=?,name=? where id=?";
    const sqlArr = [
      dic.dicTypeId,
      dic.name,
      dic.id,
    ];
    return db.queryAsync(sql, sqlArr);
  }
  remove(id) {
    const sql = "delete from sys_dictionaries_type where id=?";
    const sqlArr = [id];
    return db.queryAsync(sql, sqlArr);
  }
}
module.exports = new DictionaryTypeModel();
