'use strict';
const db = require("../config/dbPool");

class RoleModel {
  select() {
    const sql = "select * from sys_roles";
    const sqlArr = [];
    return db.queryAsync(sql, sqlArr);
  }
  insert(dic) {
    const sql =
      "insert into sys_roles(role_id,role_name) values(?,?)";
    const sqlArr = [
      dic.roleId,
      dic.roleName,
    ];
    return db.queryAsync(sql, sqlArr);
  }
  update(dic) {
    const sql =
      "update sys_roles set role_name=? where id=?";
    const sqlArr = [
      dic.roleName,
      dic.id,
    ];
    return db.queryAsync(sql, sqlArr);
  }
  remove(id) {
    const sql = "delete from sys_roles where id=?";
    const sqlArr = [id];
    return db.queryAsync(sql, sqlArr);
  }
}
module.exports = new RoleModel();
